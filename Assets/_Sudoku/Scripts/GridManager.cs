using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using com.qqwing;

public class GridManager : SingletonBehaviour<GridManager>
{
    public GameObject tilePrefab;
    public GameObject labelPrefab;

    public Transform gridContainer;
    public Transform labelContainer;

    public int size = 9;
    public float spacing = .25f;
    private float startSpacing;

    public Color[] mainColors;

    private Tile[,] grid;
    private Tile[][,] boxes;

    [Range(0f, 1f)]
    public float generationSpeed;

    internal override void Awake()
    {
        base.Awake();
        Tile.ChangedNumber += OnNumberChanged;
        QQWingMain.PuzzleGenerated += OnPuzzleGenerated;
    }

    // Start is called before the first frame update
    void Start()
    {
        startSpacing = spacing;
        StartCoroutine(CreateGrid());
    }

    /// <summary>
    /// Creates the grid
    /// </summary>
    /// <returns></returns>
    IEnumerator CreateGrid()
    {
        grid = new Tile[size, size];
        boxes = new Tile[9][,];

        for (int i = 0; i < boxes.Length; i++)
        {
            boxes[i] = new Tile[3, 3];
        }


        int currentColorIndex = 0;
        int xOffset = 0;
        int yOffset = 0;
        float xSpacing = 0;
        float ySpacing = 0;

        for (int i = 0; i < size; i++)
        {
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    Tile t = CreateTile(x + xOffset, y + yOffset, xSpacing, ySpacing, mainColors[1]);
                    t.boxNumber = i;
                    // Assign to 2D grid
                    grid[x + xOffset, y + yOffset] = t;
                    boxes[i][x, y] = t;

                    yield return new WaitForSeconds(generationSpeed);
                }
            }

            currentColorIndex += 1;
            xOffset += 3;
            xSpacing += spacing;

            //generationSpeed -= 0.01f;

            if (xOffset >= 9)
            {
                yOffset += 3;
                xOffset = 0;
                xSpacing = 0;
                ySpacing += spacing;
            }
        }
    }

    /// <summary>
    /// Creates a tile and it's accompanying label
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="xSpacing"></param>
    /// <param name="ySpacing"></param>
    /// <param name="c"></param>
    private Tile CreateTile(int x, int y, float xSpacing, float ySpacing, Color c)
    {
        GameObject tile = GameObject.Instantiate(tilePrefab, gridContainer);
        tile.name = "Tile (" + x + "," + y + ")";
        tile.transform.localPosition = new Vector2(x - 4.25f + xSpacing, y - 4.25f + ySpacing);

        GameObject label = GameObject.Instantiate(labelPrefab, labelContainer);
        label.transform.position = tile.transform.position;
        label.name = "Label (" + x + "," + y + ")";

        Tile tileComp = tile.GetComponent<Tile>();
        tileComp.x = x;
        tileComp.y = y;
        tileComp.myLabel = label.GetComponent<Text>();

        tileComp.SetColor(c);

        return tileComp;
    }

    /// <summary>
    /// When a number has changed, check the rows, columns and box that belong to that Tile to see if we need to change anything
    /// </summary>
    /// <param name="t"></param>
    void OnNumberChanged(Tile t)
    {
        wrongTiles = new List<Tile>();
        correctTiles = new List<Tile>();

        // Check every square on the grid
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                CheckRow(grid[x, y]);
                CheckColumn(grid[x, y]);
                CheckBox(grid[x, y]);
            }
        }

        // Mark everything
        for (int i = 0; i < wrongTiles.Count; i++)
        {
            if (wrongTiles[i].isValid)
                wrongTiles[i].MarkWrong();
        }

        // Only mark valid if it wasn't invalid in the first place
        for (int i = 0; i < correctTiles.Count; i++)
        {
            if (wrongTiles.Contains(correctTiles[i]) == false)
                correctTiles[i].MarkValid();
        }

        // TODO, use flood fill with a copy of the list
    }

    List<Tile> wrongTiles;
    List<Tile> correctTiles;

    /// <summary>
    /// Checks the row of the Tile given
    /// </summary>
    /// <param name="t"></param>
    void CheckRow(Tile t)
    {
        List<Tile> tiles = new List<Tile>();

        for (int i = 0; i < 9; i++)
        {
            tiles.Add(grid[i, t.y]);
        }

        for (int i = 0; i < 9; i++)
        {
            CheckDuplicates(tiles, grid[i, t.y]);
        }
    }

    /// <summary>
    /// Checks the column of the Tile given
    /// </summary>
    /// <param name="t"></param>
    void CheckColumn(Tile t)
    {
        List<Tile> tiles = new List<Tile>();

        for (int i = 0; i < 9; i++)
        {
            tiles.Add(grid[t.x, i]);
        }

        for (int i = 0; i < 9; i++)
        {
            CheckDuplicates(tiles, grid[t.x, i]);
        }
    }

    /// <summary>
    /// Checks the box for the tile given
    /// </summary>
    /// <param name="tile"></param>
    private void CheckBox(Tile tile)
    {
        Tile[,] box = boxes[tile.boxNumber];

        List<Tile> tiles = new List<Tile>();

        for (int x = 0; x < box.GetLength(0); x++)
        {
            for (int y = 0; y < box.GetLength(1); y++)
            {
                tiles.Add(box[x, y]);
            }
        }

        for (int i = 0; i < 9; i++)
        {
            CheckDuplicates(tiles, tiles[i]);
        }
    }


    /// <summary>
    /// Check whether or not the list given contains duplicates
    /// </summary>
    /// <param name="numbers"></param>
    /// <param name="t"></param>
    private void CheckDuplicates(List<Tile> numbers, Tile t)
    {
        List<Tile> duplicates = numbers.Where(other => other.myNumber == t.myNumber && other.myNumber != 0).ToList();

        if (duplicates.Count > 1)
        {
            for (int i = 0; i < duplicates.Count; i++)
            {
                if (wrongTiles.Contains(duplicates[i]) == false)
                    wrongTiles.Add(duplicates[i]);
            }
        }
        else if (correctTiles.Contains(t) == false)
            correctTiles.Add(t);
    }

    private void OnPuzzleGenerated(QQWing obj)
    {
        List<Tile> tiles = new List<Tile>();

        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                tiles.Add(grid[x, y]);
            }
        }

        int[] puzzle = obj.getPuzzle();
        for (int i = 0; i < puzzle.Length; i++)
        {
            tiles[i].myNumber = puzzle[i];

            if (tiles[i].myNumber != 0)
                tiles[i].SetInteractable(false);

            tiles[i].UpdateLabel();
        }
    }

}
