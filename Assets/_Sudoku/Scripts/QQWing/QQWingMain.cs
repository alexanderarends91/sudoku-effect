using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.qqwing
{
    public class QQWingMain : SingletonBehaviour<QQWingMain>
    {
        public QQWingOptions opts;
        DateTime startTime;

        public static event Action<QQWing> PuzzleGenerated;

        [ContextMenu("Generate puzzle")]
        public void GeneratePuzzle()
        {
            startTime = DateTime.Now;

            if (opts.action == Action.NONE)
            {
                Debug.LogError("No action was decided, please select either Action.GENERATE or Action.Solve");
            }

            QQWing ss = new QQWing();
            ss.setRecordHistory(opts.printHistory || opts.printInstructions || opts.printStats || opts.difficulty != Difficulty.UNKNOWN);
            ss.setLogHistory(opts.logHistory);
            ss.setPrintStyle(opts.printStyle);

            StartCoroutine(GeneratePuzzle(ss));
        }

        IEnumerator GeneratePuzzle(QQWing ss)
        {
            bool havePuzzle = false;

            while (!havePuzzle)
            {
                // Generate a puzzle
                if (opts.action == Action.GENERATE)
                {
                    havePuzzle = ss.generatePuzzleSymmetry(opts.symmetry);

                    if (!havePuzzle && opts.printPuzzle)
                    {
                        Debug.LogError("Could not create puzzle");
                    }
                }

                int solutions = 0;


                if (havePuzzle)
                {
                    // Count the solutions if requested.
                    // (Must be done before solving, as it would
                    // mess up the stats.)
                    if (opts.countSolutions)
                    {
                        solutions = ss.countSolutions();
                    }

                    // Solve the puzzle
                    if (opts.printSolution || opts.printHistory || opts.printStats || opts.printInstructions || opts.difficulty != Difficulty.UNKNOWN)
                    {
                        ss.solve();
                    }

                    // Bail out if it didn't meet the difficulty
                    // standards for generation
                    if (opts.action == Action.GENERATE)
                    {
                        if (opts.difficulty != Difficulty.UNKNOWN && opts.difficulty != ss.getDifficulty())
                        {
                            havePuzzle = false;
                        }
                    }
                }

                yield return new WaitForEndOfFrame();
            }

            // If we still have a puzzle
            if (havePuzzle)
            {
                TimeSpan timePassed = (DateTime.Now - startTime);
                Debug.Log("GOT PUZZLE | TIME ELAPSED: " + timePassed.ToString());
                Debug.Log(ss.getPuzzleString());
                PuzzleGenerated(ss);
            }
        }

        [Serializable]
        public class QQWingOptions
        {
            // defaults for options
            public bool printPuzzle = false;

            public bool printSolution = false;

            public bool printHistory = false;

            public bool printInstructions = false;

            public bool timer = false;

            public bool countSolutions = false;

            public Action action = Action.NONE;

            public bool logHistory = false;

            public PrintStyle printStyle = PrintStyle.READABLE;

            public int numberToGenerate = 1;

            public bool printStats = false;

            public Difficulty difficulty = Difficulty.UNKNOWN;

            public Symmetry symmetry = Symmetry.NONE;

            // int threads = Runtime.getRuntime().availableProcessors();

            public QQWingOptions() { }
        }
    }
}
