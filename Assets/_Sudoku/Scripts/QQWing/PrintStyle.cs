namespace com.qqwing
{
    public enum PrintStyle
    {
        ONE_LINE,
        COMPACT,
        READABLE,
        CSV
    }
}
