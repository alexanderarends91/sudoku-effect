using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace com.qqwing
{
    public class LogItem
    {
        /**
        * The recursion level at which this item was gathered. Used for backing out
        * log items solve branches that don't lead to a solution.
        */
        private int round;

        /**
         * The type of log message that will determine the message printed.
         */
        private LogType type;

        /**
         * Value that was set by the operation (or zero for no value)
         */
        private int value;

        /**
         * position on the board at which the value (if any) was set.
         */
        private int position;

        public LogItem(int r, LogType t)
        {
            init(r, t, 0, -1);
        }

        public LogItem(int r, LogType t, int v, int p)
        {
            init(r, t, v, p);
        }

        private void init(int r, LogType t, int v, int p)
        {
            round = r;
            type = t;
            value = v;
            position = p;
        }

        public int getRound()
        {
            return round;
        }

        /**
         * Get the type of this log item.
         */
        public LogType getType()
        {
            return type;
        }

        public void print()
        {
            Debug.Log(toString());
        }

        /**
         * Get the row (1 indexed), or -1 if no row
         */
        public int getRow()
        {
            if (position <= -1) return -1;
            return 0;// QQWing.cellToRow(position) + 1;
        }

        /**
         * Get the column (1 indexed), or -1 if no column
         */
        public int getColumn()
        {
            if (position <= -1) return -1;
            return 0;// QQWing.cellToColumn(position) + 1;
        }

        /**
         * Get the position (0-80) on the board or -1 if no position
         */
        public int getPosition()
        {
            return position;
        }

        /**
         * Get the value, or -1 if no value
         */
        public int getValue()
        {
            if (value <= 0) return -1;
            return value;
        }

        /**
         * Print the current log item. The message used is determined by the type of
         * log item.
         */
        public string getDescription()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Round: ").Append(getRound());
            sb.Append(" - ");
            sb.Append(getType().ToString());
            if (value > 0 || position > -1)
            {
                sb.Append(" (");
                if (position > -1)
                {
                    sb.Append("Row: ").Append(getRow()).Append(" - Column: ").Append(getColumn());
                }
                if (value > 0)
                {
                    if (position > -1) sb.Append(" - ");
                    sb.Append("Value: ").Append(getValue());
                }
                sb.Append(")");
            }
            return sb.ToString();
        }

        public string toString()
        {
            return getDescription();
        }
    }
}
