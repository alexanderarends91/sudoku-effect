namespace com.qqwing
{
    public enum Symmetry
    {
        NONE,
        ROTATE90,
        ROTATE180,
        MIRROR,
        FLIP,
        RANDOM
    }
}
