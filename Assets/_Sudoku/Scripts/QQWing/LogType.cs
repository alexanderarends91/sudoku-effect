namespace com.qqwing
{
    public enum LogType
    {
        GIVEN,
        SINGLE,
        HIDDEN_SINGLE_ROW,
        HIDDEN_SINGLE_COLUMN,
        HIDDEN_SINGLE_SECTION,
        GUESS,
        ROLLBACK,
        NAKED_PAIR_ROW,
        NAKED_PAIR_COLUMN,
        NAKED_PAIR_SECTION,
        POINTING_PAIR_TRIPLE_ROW,
        POINTING_PAIR_TRIPLE_COLUMN,
        ROW_BOX,
        COLUMN_BOX,
        HIDDEN_PAIR_ROW,
        HIDDEN_PAIR_COLUMN,
        HIDDEN_PAIR_SECTION
    }
}
