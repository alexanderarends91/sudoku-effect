using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SpriteGlow;

public class Tile : MonoBehaviour
{
    public static event Action<Tile> ChangedNumber;

    internal int x;
    internal int y;

    private SpriteRenderer mySpriteRenderer;
    private SpriteGlowEffect glow;

    internal Text myLabel;

    internal int myNumber;
    private Color myColor;
    private Color glowColor;
    private Color ogColor;

    [Range(0.2f, 1f)]
    public float colorDiff;
    internal int boxNumber;

    private Vector2 startPos;
    internal bool isValid;
    private bool interactable;
    private bool complete;

    private void Awake()
    {
        mySpriteRenderer = this.GetComponent<SpriteRenderer>();
        glow = this.GetComponent<SpriteGlowEffect>();

        Tile.ChangedNumber += OnChangedNumber;
        ogColor = Color.clear;

        isValid = true;
        interactable = true;
    }

    internal void SetColor(Color mainColor, bool setWhite = true)
    {
        if (!interactable)
        {
            mainColor = Color.black;
            glowColor = Color.white;
        }

        if (setWhite)
        {
            mySpriteRenderer.color = Color.white;
            glow.GlowColor = Color.white;
        }

        Color newColor = mainColor * UnityEngine.Random.Range(1f - colorDiff, 1f + colorDiff);

        float h = 0;
        float s = 0;
        float v = 0;
        Color.RGBToHSV(mainColor, out h, out s, out v);
        s = 0.98f;

        glow.GlowColor = Color.HSVToRGB(h, s, v);
        glowColor = glow.GlowColor;

        if (!interactable)
            glow.GlowColor = Color.white;


        myColor = newColor;

        if (ogColor == Color.clear)
            ogColor = newColor;

        myColor.a = .5f;
        mySpriteRenderer.DOColor(myColor, .2f).SetEase(Ease.InOutSine);

        startPos = this.transform.localPosition;
    }

    internal void UpdateLabel()
    {
        myLabel.text = myNumber == 0 ? "" : myNumber.ToString();

        if (myNumber != 0)
        {
            myLabel.color = isValid ? Color.white : Color.red;
            glow.GlowColor = isValid ? glowColor : Color.red;
            glow.GlowColor = !interactable ? Color.white : glowColor;

            if (!interactable)
                glow.OutlineWidth = 1;
        }
    }

    public void Clicked(int mouseButton)
    {
        if (interactable == false)
            return;

        DOTween.Kill(mySpriteRenderer);
        mySpriteRenderer.color = Color.white;

        mySpriteRenderer.DOColor(myColor, .2f).SetEase(Ease.InOutSine);

        if (mouseButton == 0)
        {
            myNumber += 1;

            if (myNumber > 9)
                myNumber = 0;
        }
        else if (mouseButton == 1)
        {
            myNumber -= 1;

            if (myNumber < 1)
                myNumber = 9;

            glow.GlowColor = glowColor;
        }
        else if (mouseButton == 2)
        {
            myNumber = 0;
        }

        if (myNumber == 0)
        {
            isValid = true;
            complete = false;
            SetColor(ogColor);
        }

        ChangedNumber?.Invoke(this);

        UpdateLabel();
    }

    private void Update()
    {
        myLabel.transform.position = this.transform.position;
    }

    private void OnChangedNumber(Tile obj)
    {
        if (obj == this || interactable == false)
            return;

        float dist = Vector3.Distance(obj.transform.localPosition, this.transform.localPosition);
        Vector2 direction = this.transform.localPosition - obj.transform.localPosition;
        float strength = 1f / dist;
        direction = -direction.normalized * .4f * strength;

        mySpriteRenderer.DOColor(myColor * (1 + strength), .25f).SetDelay(0.1f * dist);
        mySpriteRenderer.DOColor(myColor, .25f).SetDelay(.25f + (0.1f * dist)).SetEase(Ease.InOutSine);
    }


    public void OnMouseEnter()
    {
        if (interactable == false)
            return;

        DOTween.Kill(mySpriteRenderer);
        mySpriteRenderer.color = myColor * 2f;
        mySpriteRenderer.DOColor(myColor, .5f).SetEase(Ease.InOutSine);
    }

    internal void MarkComplete()
    {
        complete = true;

        SetColor(Color.green, false);

        UpdateLabel();

    }

    internal void MarkWrong()
    {
        isValid = false;
        complete = false;

        SetColor(Color.red, false);

        UpdateLabel();
    }

    internal void MarkValid()
    {
        if (!isValid)
        {
            isValid = true;
            SetColor(ogColor, false);
            UpdateLabel();
        }
    }

    internal void SetInteractable(bool interactable)
    {
        this.interactable = interactable;

        SetColor(ogColor);
    }
}
