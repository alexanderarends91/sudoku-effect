using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public LayerMask stuffToHit;
    public Transform toRotate;
    public Vector3 rotation;
    public float rotSpeed;

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
            CastRay(0);

        if (Input.GetMouseButtonUp(1))
            CastRay(1);

        if (Input.GetMouseButtonUp(2))
            CastRay(2);

        Vector2 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -10));
        
        float yStrength = (Vector2.zero - pos).normalized.y;
        float xStrength = (Vector2.zero - pos).normalized.x;

        toRotate.rotation = Quaternion.Lerp(toRotate.rotation, Quaternion.Euler(rotation.x * yStrength, rotation.y * xStrength, 0f), Time.deltaTime * rotSpeed);
    }

    void CastRay(int mouseButton)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            hit.collider.GetComponent<Tile>().Clicked(mouseButton);
        }
    }
}
